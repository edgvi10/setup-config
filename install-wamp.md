
## Installing WAMP on Windows 10

Recomendations:
- [Cmder](https://cmder.net/) as a command line.
- Install [GIT](https://git-scm.com/) for bash commands.
- Install [Node.js](https://nodejs.org/) and NPM in stable versions.
- Create a directory to host config files. Eg. `C:\wamp`, `C:\localhost`, etc.

For these config I define `C:\wamp` as __INSTALL_DIR__ directory.

### Config Apache 2.4

1. Download binary files from [Apache Lounge](https://www.apachelounge.com) and extract in `{INSTALL_DIR}\apache`.

2. Define SRVROOT to root directory of Apache `"{INSTALL_DIR}/apache"` (as exact).

3. Find _"Listen"_ and add the _ports_ you like to use, `Listen 80`.
    
4. Find `rewrite_module` and remove "#" from this line.

5. Find `Virtual Hosts` conf file and remove "#" from this line to allow then.

6. Open your command line and access _"System Variables"_.
	```
	sysdm.cpl
	```
	> Includes `INSTALL_DIR\apache\bin` to _PATH_ order to access _ApacheMonitor_ from anywhere by command line.


7. To start Apache _as service_ first time, type in command line
	```
	httpd.exe -k start
	```


### Install and config PHP (7.x)

1. Download [PHP](https://windows.php.net/download/) zip files.

2. Extract files in `INSTALL_DIR\php\7.x\`.

3. Create a `php.ini` file or copy from `php.ini-[development|production]` files.
	> My suggested config `php.ini` file is in `/wamp` directory in this repository.

4. Add following lines to `httpd.conf` in Apache
	```
	PHPIniDir "INSTALL_DIR/php/7.x"
	AddHandler application/x-httpd-php .php
	LoadModule php7_module "INSTALL_DIR/php/7.x/php7apache2_4.dll"
	```

5. Find the `dir_module` in `httpd.conf` and edit with follow lines to order
	```
	<IfModule dir_module>
	    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
	</IfModule>
	```

### Install MySQL

1. Download [MySQL](https://dev.mysql.com/downloads/mysql/) zip files.

2. Extract files in `INSTALL_DIR\mysql\`.

3. Create a `data` directory in root of mysql.

4. Create a `my.ini` file in `bin` directory.
	> My suggested config `my.ini`  file is in `/wamp` directory in this repository.

5. Execute on console
	```
	INSTALL_DIR\mysql\bin\mysqld --console --initialize-insecure
	INSTALL_DIR\mysql\bin\mysqld --install MySQL
	net start MySQL
	INSTALL_DIR\mysql\bin\mysql -u root
	```
	> Optional set `INSTALL_DIR\mysql\bin`  _PATH_ order to access _mysql & mysqld_ from anywhere by command line.
	
6. Set a `root` password
	```
	ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
	FLUSH PRIVILEGES;
	```
---
Install Adminer to admin your database with a light weight tool.
	- Adminer Oficial [https://github.com/vrana/adminer/](https://github.com/vrana/adminer/)
	- Adminer Custom by @[edgvi10](https://bitbucket.org/edgvi10/adminer-custom/src/master/)