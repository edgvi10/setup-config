# INSTALL AND CONFIGURE _LAMP_

Install __Apache__ and __cURL__.

	sudo apt update

	sudo apt install apache2 curl


Configure __Apache__

	sudo ufw app info "Apache Full"

	sudo ufw allow in "Apache Full"


Install __Node.js__ 10.x and __NPM__

	curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

	sudo apt-get install -y nodejs

	sudo apt install npm


Install __MySQL__

	sudo apt install mysql-server

	sudo mysql_secure_installation


Configure _root_ password

	sudo mysql

	ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'NEW_PASSWORD';

	FLUSH PRIVILEGES;


Install __PHP__ _(Laravel friendly)_

	sudo apt install php libapache2-mod-php php-mysql php-curl php-cli php-xml php-mbstring php-xmlrpc php-intl php-zip php-gd
	
Configure __dir.conf__

	sudo nano /etc/apache2/mods-enabled/dir.conf

	<IfModule mod_dir.c>
	   	DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
	</IfModule>


Enable `rewrite`

	sudo a2enmod rewrite

	
Restart and check _Apache_ status

	sudo systemctl restart apache2 && sudo systemctl status apache2


Install __GIT__

	sudo add-apt-repository ppa:git-core/ppa && sudo apt update && sudo apt install git

	sudo git config --global user.name 'Full Name'

	sudo git config --global user.email 'email@server.com'


Install my custom Adminer for MySQL [repo](https://bitbucket.org/edgvi10/adminer-custom/)

	cd /var/www/html

	git clone https://bitbucket.org/edgvi10/adminer-custom.git adminer