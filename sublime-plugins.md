## Personalization & Produtivity
- Emmet
- SFTP
- Trimmer
- CSS Format
- Color Highlights
- All Autocomplete
- Material Theme (include File Icon)
- Material Theme Appbar

### Syntaxes
- JQuery
- HTML5
- CSS3
- SCSS
- TypeScript
- INI
- Apache Conf
