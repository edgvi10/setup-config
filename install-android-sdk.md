## Instaling Android SDK Tools

Download latest [Android SDK Tools Zip](https://developer.android.com/studio/#downloads)

Install latest JDK

    sudo apt install default-jdk
    
Install unzip if not installed yet

    sudo apt install unzip
    
Unzip and move to '~' the download .zip file

    unzip sdk-tools-linux-[version].zip
    mv tools ~/tools
    cd ~
    mkdir android-sdk
    mv tools android-sdk/tools

Then add the Android SDK to your PATH
    
    sudo gedit ~/.bashrc

Add these lines on end of file

    # Export the Android SDK path 
    export ANDROID_HOME=$HOME/android-sdk
    export PATH=$PATH:$ANDROID_HOME/tools/bin
    export PATH=$PATH:$ANDROID_HOME/platform-tools

    # Fixes sdkmanager error with java versions higher than java 8
    export JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'


Run

    source ~/.bashrc


To list platforms and components
    
    sdkmanager --list


To install any components

    sdkmanager "platform-tools" "platforms;android-29"


